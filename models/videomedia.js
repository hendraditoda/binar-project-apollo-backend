'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class VideoMedia extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      VideoMedia.belongsTo(models.user, {
        foreignKey: 'user_id',
        onDelete: 'CASCADE',
      });
    }
  }
  VideoMedia.init(
    {
      name: DataTypes.STRING,
      originalName: DataTypes.STRING,
      description: DataTypes.STRING,
      videoPath: DataTypes.STRING,
      size: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: 'VideoMedia',
    }
  );
  return VideoMedia;
};
