const authenticationController = require('../controllers/authentication.controller');
const { user } = require('../models');

describe('User Controller', () => {
  describe('Login', () => {
    it('should return success response on successful login', async () => {
      const req = {
        body: {
          username: 'testuser',
          password: 'testpassword',
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const findOneMock = jest.spyOn(user, 'findOne').mockResolvedValue({
        validPassword: jest.fn().mockReturnValue(true),
        generateToken: jest.fn().mockReturnValue('mock-token'),
      });

      await authenticationController.login(req, res);

      expect(findOneMock).toBeCalledWith({
        where: {
          username: 'testuser',
        },
      });
      expect(res.status).toBeCalledWith(200);
      expect(res.json).toBeCalledWith({
        message: 'Login sucess',
        data: {
          token: 'mock-token',
        },
      });

      findOneMock.mockRestore();
    });

    it('should return error response on failed login', async () => {
      const req = {
        body: {
          username: 'testuser',
          password: 'testpassword',
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const findOneMock = jest.spyOn(user, 'findOne').mockResolvedValue({
        validPassword: jest.fn().mockReturnValue(false),
      });

      await authenticationController.login(req, res);

      expect(findOneMock).toBeCalledWith({
        where: {
          username: 'testuser',
        },
      });
      expect(res.status).toBeCalledWith(400);
      expect(res.json).toBeCalledWith({
        error: {
          message: 'Login failed, username/password does not match',
          data: {},
        },
      });

      findOneMock.mockRestore();
    });

    it('should return error response on database error', async () => {
      const req = {
        body: {
          username: 'testuser',
          password: 'testpassword',
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const findOneMock = jest.spyOn(user, 'findOne').mockRejectedValue(new Error('Database error'));

      await authenticationController.login(req, res);

      expect(findOneMock).toBeCalledWith({
        where: {
          username: 'testuser',
        },
      });
      expect(res.status).toBeCalledWith(400);
      expect(res.json).toBeCalledWith({
        error: {
          message: 'Database error',
          data: {},
        },
      });

      findOneMock.mockRestore();
    });
  });

  describe('Register', () => {
    it('should return success response on successful registration', async () => {
      const req = {
        body: {
          username: 'testuser',
          password: 'testpassword',
          email: 'test@example.com',
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const createMock = jest.spyOn(user, 'create').mockResolvedValue({});

      await authenticationController.register(req, res);

      expect(createMock).toBeCalledWith({
        username: 'testuser',
        password: 'testpassword',
        email: 'test@example.com',
        isAdmin: false,
      });
      expect(res.status).toBeCalledWith(201);
      expect(res.json).toBeCalledWith({
        message: 'Data User Create Successfully',
        data: {},
      });

      createMock.mockRestore();
    });

    it('should return error response on database error', async () => {
      const req = {
        body: {
          username: 'testuser',
          password: 'testpassword',
          email: 'test@example.com',
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const createMock = jest.spyOn(user, 'create').mockRejectedValue(new Error('Database error'));

      await authenticationController.register(req, res);

      expect(createMock).toBeCalledWith({
        username: 'testuser',
        password: 'testpassword',
        email: 'test@example.com',
        isAdmin: false,
      });
      expect(res.status).toBeCalledWith(400);
      expect(res.json).toBeCalledWith({
        error: {
          message: 'Database error',
          data: {},
        },
      });

      createMock.mockRestore();
    });
  });

  describe('Verify', () => {
    it('should return success response', async () => {
      const req = {
        params: {
          token: 'mock-token',
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      await authenticationController.verify(req, res);

      expect(res.status).toBeCalledWith(200);
      expect(res.json).toBeCalledWith({
        message: 'OK',
        data: {},
      });
    });
  });
});
