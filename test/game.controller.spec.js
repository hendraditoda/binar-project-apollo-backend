const gameController = require('../controllers/game.controller');
const { game } = require('../models');

const req = {};
const res = {
  status: jest.fn().mockReturnThis(),
  json: jest.fn(),
};

describe('Game Controller', () => {
  describe('gameList', () => {
    test('should return 200 success response get data', async () => {
      req.params = {
        id: 1,
      };
      const mockGameDatas = [
        {
          id: 1,
          name: 'tetris',
          description: 'tetris joy',
          thumbnail_url: 'zzz',
          isPlayed: 'zzz',
          play_count: 'zzz',
        },
      ];
      game.findAll = jest.fn().mockResolvedValue(mockGameDatas);
      await gameController.gameList(req, res);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith({
        message: 'Success Get Data',
        data: {
          game_list: mockGameDatas,
        },
      });
    });
    test('should return 400 error null', async () => {
      const mockError = new Error('Error');
      req.params = {
        id: 1000,
      };
      const mockGameDatas = 'NULL';
      game.findAll = jest.fn().mockReturnValue(mockError);
      await gameController.gameList(req, res);
      expect(res.status).toHaveBeenCalledWith(400);
      expect(res.json).toHaveBeenCalledWith({
        error: {
          message: 'Error',
          data: mockGameDatas,
        },
      });
    });
  });

  describe('createGame', () => {
    test('should return 200 create data game', async () => {
      const mockGameDatas = {
        id: 2,
        name: 'tetris',
        description: 'tetris joy',
        thumbnail_url: 'zzz',
        isPlayed: 'zzz',
        play_count: 'zzz',
      };
      const req = {
        params: {
          id: 1,
        },
        body: {
          name: 'tetris',
          description: 'tetris joy',
          thumbnail_url: 'zzz',
          isPlayed: 'zzz',
          play_count: 'zzz',
        },
      };
      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };
      const createMock = jest.spyOn(game, 'create').mockResolvedValue(mockGameDatas);
      await gameController.createGame(req, res);
      expect(createMock).toHaveBeenCalledWith({
        name: 'tetris',
        description: 'tetris joy',
        thumbnail_url: 'zzz',
        isPlayed: 'zzz',
        play_count: 'zzz',
      });
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith({
        message: 'Data Create Successfully',
        data: mockGameDatas,
      });
      createMock.mockRestore();
    });
  });

  describe('updateGame', () => {
    beforeEach(() => {
      jest.restoreAllMocks();
    });
    test('should return 200 update game', async () => {
      const mockNewGameDatas = {
        isPlayed: true,
      };

      const req = {
        params: {
          idGame: 1,
        },
        body: {
          isPlayed: true,
        },
      };
      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const updateMock = jest.spyOn(game, 'update').mockResolvedValue(mockNewGameDatas);

      await gameController.updateGame(req, res);

      expect(updateMock).toHaveBeenCalledWith(
        {
          isPlayed: true,
        },
        { where: { id: 1 } }
      );
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith({
        message: 'Data Changed Successfully',
        data: mockNewGameDatas,
      });

      updateMock.mockRestore();
    });
  });
});
