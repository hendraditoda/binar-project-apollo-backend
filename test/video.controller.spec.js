const fs = require("fs");
const VideoController = require("../controllers/video.controller");
const { VideoMedia } = require("../models");

const req = {};
const res = {
  status: jest.fn().mockReturnThis(),
  json: jest.fn(),
};

// const fs = jest.genMockFromModule("fs");

describe("Video Controller", () => {
  describe("GET /api/videoMedia/:id", () => {
    test("should return 201 status code with data", async () => {
      req.params = {
        id: 1,
      };
      const mockVideoDatas = [
        {
          id: 1,
          name: "video-1687871136941.mp4",
          originalName: "109_import_FPpreview.mp4",
          description: "video lucu",
          videoPath: "public/video/video-1687871136941.mp4",
          size: 3912909,
          createdAt: "2023-06-27T13:05:37.125Z",
          updatedAt: "2023-06-27T13:05:37.125Z",
          user_id: 1,
        },
      ];
      VideoMedia.findAll = jest.fn().mockResolvedValue(mockVideoDatas);

      await VideoController.index(req, res);

      expect(res.status).toHaveBeenCalledWith(201);
      expect(res.json).toHaveBeenCalledWith({
        message: "Success Get Data",
        data: {
          videos: mockVideoDatas,
        },
      });
    });
    test("should return 401 status code with data null if empty/data not found", async () => {
      const mockError = new Error("Data not found");
      req.params = {
        id: 1000,
      };
      const mockVideoDatas = "NULL";
      VideoMedia.findAll = jest.fn().mockReturnValue(mockError);
      await VideoController.index(req, res);
      expect(res.status).toHaveBeenCalledWith(401);
      expect(res.json).toHaveBeenCalledWith({
        error: {
          message: "Data not found",
          data: mockVideoDatas,
        },
      });
    });
  });

  describe("POST /api/videoMedia/:id", () => {
    test("Should return 201 and Create Data Video ", async () => {
      const mockVideoDatas = {
        id: 2,
        name: "video-1688058675897.mp4",
        originalName: "_109__import_FPpreview.mp4",
        description: "video lucu",
        videoPath: "public/video/video-1688058675897.mp4",
        size: 3912909,
        user_id: 1,
        updatedAt: "2023-06-29T17:11:15.966Z",
        createdAt: "2023-06-29T17:11:15.966Z",
      };

      const req = {
        params: {
          id: 1,
        },
        body: {
          description: "video lucu",
        },
        file: {
          filename: "video-1688058675897.mp4",
          originalname: "_109__import_FPpreview.mp4",
          path: "public/video/video-1688058675897.mp4",
          size: 3912909,
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const createMock = jest
        .spyOn(VideoMedia, "create")
        .mockResolvedValue(mockVideoDatas);

      await VideoController.create(req, res);

      expect(createMock).toBeCalledWith({
        name: "video-1688058675897.mp4",
        originalName: "_109__import_FPpreview.mp4",
        description: "video lucu",
        videoPath: "public/video/video-1688058675897.mp4",
        size: 3912909,
        user_id: 1,
      });
      expect(res.status).toBeCalledWith(201);
      expect(res.json).toBeCalledWith({
        message: "Data Video Create Successfully",
        data: mockVideoDatas,
      });

      createMock.mockRestore();
    });
  });

  describe("PUT /api/videoMedia/update/:id", () => {
    beforeEach(() => {
      jest.restoreAllMocks();
    });
    test("Should return 201, Update Data Video and Delete the old ones ", async () => {
      const mockOneVideoDatas = { name: "video-1688058675897.mp4" };
      const mockNewVideoDatas = {
        name: "video-1688058675897.mp4",
        originalName: "_109__import_FPpreview.mp4",
        description: "video lucu",
        videoPath: "public/video/video-1688058675897.mp4",
        size: 3912909,
      };

      const req = {
        params: {
          id: 1,
        },
        body: {
          description: "video lucu",
        },
        file: {
          filename: "video-1688058675897.mp4",
          originalname: "_109__import_FPpreview.mp4",
          path: "public/video/video-1688058675897.mp4",
          size: 3912909,
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };
      const findOneMock = jest
        .spyOn(VideoMedia, "findOne")
        .mockResolvedValue(mockOneVideoDatas);

      const unlinkMock = jest
        .spyOn(fs, "unlink")
        .mockImplementation((path, callback) => {
          callback(null); // Simulate successful file deletion
        });

      const updateMock = jest
        .spyOn(VideoMedia, "update")
        .mockResolvedValue(mockNewVideoDatas);

      await VideoController.updates(req, res);

      expect(findOneMock).toBeCalledWith({ where: { id: 1 } });
      expect(unlinkMock).toBeCalledWith(
        __basedir + "/public/video/" + mockOneVideoDatas.name,
        expect.any(Function)
      );
      expect(updateMock).toBeCalledWith(
        {
          description: "video lucu",
          name: "video-1688058675897.mp4",
          originalName: "_109__import_FPpreview.mp4",
          size: 3912909,
          videoPath: "public/video/video-1688058675897.mp4",
        },
        { where: { id: 1 } }
      );
      expect(res.status).toBeCalledWith(200);
      expect(res.json).toBeCalledWith({
        message: "Data Changed Successfully",
        data: mockNewVideoDatas,
      });
      findOneMock.mockRestore();
      updateMock.mockRestore();
      unlinkMock.mockRestore();
    });
  });

  describe("DELETE /api/videoMedia/delete/:id", () => {
    beforeEach(() => {
      jest.restoreAllMocks();
    });

    test("Should return 200 and delete data successfully", async () => {
      const mockOneVideoDatas = { name: "video-1688058675897.mp4" };
      const mockVideoDatas = {
        name: "video-1688058675897.mp4",
        originalName: "_109__import_FPpreview.mp4",
        description: "video lucu",
        videoPath: "public/video/video-1688058675897.mp4",
        size: 3912909,
      };

      req.params = {
        id: 1,
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const findOneMock = jest
        .spyOn(VideoMedia, "findOne")
        .mockResolvedValue(mockOneVideoDatas);

      const unlinkMock = jest
        .spyOn(fs, "unlink")
        .mockImplementation((path, callback) => {
          callback(null); // Simulate successful file deletion
        });

      const destroyMock = jest
        .spyOn(VideoMedia, "destroy")
        .mockResolvedValue(1); // Simulate 1 row affected

      await VideoController.deletes(req, res);

      expect(findOneMock).toBeCalledWith({ where: { id: req.params.id } });
      expect(unlinkMock).toBeCalledWith(
        __basedir + "/public/video/" + mockOneVideoDatas.name,
        expect.any(Function)
      );
      expect(destroyMock).toBeCalledWith({ where: { id: req.params.id } });
      expect(res.status).toBeCalledWith(200);
      expect(res.json).toBeCalledWith({
        message: "Data Deleted Successfully",
        data: {},
      });

      findOneMock.mockRestore();
      unlinkMock.mockRestore();
      destroyMock.mockRestore();
    });
  });
});
