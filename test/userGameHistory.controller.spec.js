const UserGameHistoryController = require('../controllers/userGameHistory.controller');
const { user_game_history } = require('../models');

const req = {};
const res = {
  status: jest.fn().mockReturnThis(),
  json: jest.fn(),
};

describe('User Game History', () => {
  describe('index', () => {
    test('should return 200 success', async () => {
      req.params = {
        id: 1,
      };
      const mockUserGameHistory = {
        id: 1,
        time: '123',
        score: '123',
        createdAt: '2023-07-02T13:44:42.529Z',
        updatedAt: '2023-07-02T13:44:42.529Z',
        id_game: 1,
        user_id: 1,
        user: {
          id: 1,
          email: 'kakekzeus@gmail.com',
          username: 'kakekzeus',
          password: '$2b$10$bIeFKDWz03qsYoXKkU4v3O0q1Y4S6Ew8uhdM8WtO5aDgO.peShHxW',
          isAdmin: false,
          isDelete: null,
          createdAt: '2023-06-23T10:48:28.475Z',
          updatedAt: '2023-06-23T10:48:28.475Z',
        },
        game: {
          id: 1,
          name: 'gunting batu kertas',
          description: 'abc',
          thumbnail_url: 'abc',
          isPlayed: true,
          play_count: 1,
          createdAt: '2023-07-02T13:39:02.653Z',
          updatedAt: '2023-07-02T13:39:16.256Z',
          user_id: null,
        },
      };
      user_game_history.findAll = jest.fn().mockResolvedValue(mockUserGameHistory);
      await UserGameHistoryController.index(req, res);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith({
        message: 'Success Get All Data from server',
        data: { usersHistoryData: mockUserGameHistory },
      });
    });
    // test('should return 400 error', async () => {
    //   const mockError = new Error('Server Error');
    //   req.params = {
    //     id: 1000,
    //   };
    //   const mockUserGameHistory = 'NULL';
    //   user_game_history.findOne = jest.fn().mockResolvedValue(mockError);
    //   await UserGameHistoryController.index(req, res);
    //   expect(res.status).toHaveBeenCalledWith(400);
    //   expect(res.json).toHaveBeenCalledWith({
    //     error: {
    //       message: 'Server Error',
    //       data: mockUserGameHistory,
    //     },
    //   });
    // });
  });

  describe('show', () => {
    test('should return 200 success', async () => {
      const mockUserGameHistoris = [
        {
          id: 1,
          time: '123',
          score: '123',
          createdAt: '2023-07-02T13:44:42.529Z',
          updatedAt: '2023-07-02T13:44:42.529Z',
          id_game: 1,
          user_id: 1,
        },
      ];
      const req = {
        params: {
          id: 1,
        },
      };
      const findOneMock = jest.spyOn(user_game_history, 'findOne').mockResolvedValue(mockUserGameHistoris);

      await UserGameHistoryController.show(req, res);
      expect(findOneMock).toBeCalledWith({ where: { user_id: 1 } });
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith({
        message: 'Success Get Data',
        data: { userData: mockUserGameHistoris },
      });
    });

    test('should return 400 error', async () => {
      const mockErrors = new Error('User not found');
      req.params = {
        id: 1000,
      };
      const mockUserGameHistoris = 'NULL';
      user_game_history.findOne = jest.fn().mockResolvedValue(mockErrors);
      await UserGameHistoryController.show(req, res);
      expect(res.status).toHaveBeenCalledWith(400);
      expect(res.json).toHaveBeenCalledWith({
        error: {
          message: 'User not found',
          data: mockUserGameHistoris,
        },
      });
    });
  });

  describe('create', () => {
    test('should return 200', async () => {
      const mockUserGameHistories = {
        id: 1,
        time: '123',
        score: '123',
        user_id: 1,
        id_game: 1,
        updatedAt: '2023-07-02T13:44:42.529Z',
        createdAt: '2023-07-02T13:44:42.529Z',
      };
      const req = {
        params: {
          id: 'userId',
          idgame: 'idGame',
        },
        body: {
          time: 10,
          score: 100,
        },
      };
      const findOneMock = jest.spyOn(user_game_history, 'findOne').mockResolvedValue(null);
      const createMock = jest.spyOn(user_game_history, 'create').mockResolvedValue(mockUserGameHistories);
      await UserGameHistoryController.create(req, res);
      expect(findOneMock).toHaveBeenCalledWith({ where: { user_id: 'userId' } });
      expect(createMock).toHaveBeenCalledWith({
        time: 10,
        score: 100,
        user_id: 'userId',
        id_game: 'idGame',
      });
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith({
        message: 'Success Create Data',
        data: {
          userData: mockUserGameHistories,
        },
      });
      createMock.mockRestore();
    });
    // test('should return 400 error', async () => {
    //   const req = {
    //     params: {
    //       id: 'userId',
    //       idgame: 'idGame',
    //     },
    //     body: {
    //       time: 10,
    //       score: 100,
    //     },
    //   };
    //   const mockUserGameHistories = 'NULL';
    //   const findOneMock = jest.spyOn(user_game_history, 'findOne').mockResolvedValue(new Error('Server Error'));
    //   await UserGameHistoryController.create(req, res);
    //   expect(findOneMock).toHaveBeenCalledWith({ where: { user_id: 'userId' } });
    //   expect(res.status).toHaveBeenCalledWith(400);
    //   expect(res.json).toHaveBeenCalledWith({
    //     error: {
    //       message: 'Server Error',
    //       data: mockUserGameHistories,
    //     },
    //   });
    //   findOneMock.mockRestore();
    // });
  });
});
