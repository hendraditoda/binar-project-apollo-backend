const photoController = require('../controllers/photo.controller');
const PhotoController = require('../controllers/photo.controller');
const { PhotoMedia } = require('../models');
const req = {};
const res = {
  status: jest.fn().mockReturnThis(),
  json: jest.fn(),
};
// const multer = require('multer');
// const path = require('path');
// const directoryPath = path.resolve('public/photo/');
const fs = require('fs');

describe('photoMediaController', () => {
  describe('index', () => {
    it('should return 201 status code with data', async () => {
      req.params = {
        id: 1,
      };
      const mockPhotoDatas = [
        {
          id: 1,
          name: 'photo-1687621179226.png',
          originalName: 'photo-1687621179226.png',
          description: 'photo lucu',
          photoPath: 'public/photo/photo-1687621179226.png',
          size: 200,
          createdAt: '2023-06-27T13:05:37.125Z',
          updatedAt: '2023-06-27T13:05:37.125Z',
          user_id: 1,
        },
      ];

      PhotoMedia.findAll = jest.fn().mockResolvedValue(mockPhotoDatas);

      await PhotoController.index(req, res);

      expect(res.status).toHaveBeenCalledWith(201);
      expect(res.json).toHaveBeenCalledWith({
        message: 'Success Get Data',
        data: {
          photos: mockPhotoDatas,
        },
      });
    });
    it('should return 401 status code with data null if empty/data not found', async () => {
      const mockError = new Error('Data not Found');
      req.params = {
        id: 1000,
      };
      const mockPhotoDatas = 'NULL';
      PhotoMedia.findAll = jest.fn().mockReturnValue(mockError);
      await PhotoController.index(req, res);
      expect(res.status).toHaveBeenCalledWith(401);
      expect(res.json).toHaveBeenCalledWith({
        error: {
          message: 'Data Not Found',
          data: mockPhotoDatas,
        },
      });
    });
  });

  describe('Upload', () => {
    test('Should return 201 and Create Data Photo ', async () => {
      const mockPhotoDatas = {
        id: 2,
        name: 'photo-1687621179226.png',
        originalName: 'photo-1687621179226.png',
        description: 'photo lucu',
        photoPath: 'public/photo/photo-1687621179226.png',
        size: 200,
        user_id: 1,
        updatedAt: '2023-06-29T17:11:15.966Z',
        createdAt: '2023-06-29T17:11:15.966Z',
      };

      const req = {
        params: {
          id: 1,
        },
        body: {
          description: 'photo lucu',
        },
        file: {
          filename: 'photo-1687621179226.png',
          originalname: 'photo-1687621179226.png',
          path: 'public/photo/photo-1687621179226.png',
          size: 200,
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const createMock = jest.spyOn(PhotoMedia, 'create').mockResolvedValue(mockPhotoDatas);

      await PhotoController.create(req, res);

      expect(createMock).toBeCalledWith({
        name: 'photo-1687621179226.png',
        originalName: 'photo-1687621179226.png',
        description: 'photo lucu',
        photoPath: 'public/photo/photo-1687621179226.png',
        size: 200,
        user_id: 1,
      });
      expect(res.status).toBeCalledWith(201);
      expect(res.json).toBeCalledWith({
        message: 'Data Photo Create Successfully',
        data: mockPhotoDatas,
      });

      createMock.mockRestore();
    });
  });

  describe('Delete', () => {
    beforeEach(() => {
      jest.restoreAllMocks();
    });

    test('Should return 200 and delete data successfully', async () => {
      const mockPhotoId = 1;
      // const mockPhotoData = { id: mockPhotoId, name: 'photo-1234567890.jpg' };
      const mockPhotoData = {
        name: 'photo-1234567890.jpg',
        originalName: 'photo-1234567890.jpg',
        description: 'photo lucu',
        videoPath: 'public/photo/photo-1234567890.jpg',
        size: 200,
      };

      const req = {
        params: {
          id: mockPhotoId,
          body: {
            description: 'photo lucu',
          },
          file: {
            filename: 'video-1688058675897.mp4',
            originalname: '_109__import_FPpreview.mp4',
            path: 'public/video/video-1688058675897.mp4',
            size: 3912909,
          },
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const findOneMock = jest.spyOn(PhotoMedia, 'findOne').mockResolvedValue(mockPhotoData);

      const unlinkMock = jest.spyOn(fs, 'unlink').mockImplementation((path, callback) => {
        callback(null); // Simulate successful file deletion
      });

      const destroyMock = jest.spyOn(PhotoMedia, 'destroy').mockResolvedValue(1); // Simulate 1 row affected

      await PhotoController.deletes(req, res);

      expect(findOneMock).toBeCalledWith({ where: { id: mockPhotoId } });
      expect(unlinkMock).toBeCalledWith(__basedir + '/public/photo/' + mockPhotoData.name, expect.any(Function));
      expect(destroyMock).toBeCalledWith({ where: { id: mockPhotoId } });
      expect(res.status).toBeCalledWith(200);
      expect(res.json).toBeCalledWith({
        message: 'Data Deleted Successfully',
        data: {},
      });

      findOneMock.mockRestore();
      unlinkMock.mockRestore();
      destroyMock.mockRestore();
    });
  });

  describe('Update', () => {
    test('Should return 200, Update Data Photo and Delete the old ones', async () => {
      const mockOnePhotoData = { name: 'photo-1234567890.jpg' };
      const mockNewPhotoData = {
        name: 'photo-1234567890.jpg',
        originalName: 'photo.jpg',
        description: 'Photo description',
        photoPath: 'public/photo/photo-1234567890.jpg',
        size: 12345,
      };

      const req = {
        params: {
          id: 1,
        },
        body: {
          name: 'photo-1234567890.jpg',
          description: 'Photo description',
        },
        file: {
          filename: 'photo-1234567890.jpg',
          originalname: 'photo.jpg',
          path: 'public/photo/photo-1234567890.jpg',
          size: 12345,
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const findOneMock = jest.spyOn(PhotoMedia, 'findOne').mockResolvedValue(mockOnePhotoData);

      const unlinkMock = jest.spyOn(fs, 'unlink').mockImplementation((path, callback) => {
        callback(null); // Simulate successful file deletion
      });

      const updateMock = jest.spyOn(PhotoMedia, 'update').mockResolvedValue([1]); // Mock the number of rows affected by the update

      await PhotoController.updates(req, res);

      expect(findOneMock).toBeCalledWith({ where: { id: 1 } });
      expect(unlinkMock).toBeCalledWith(__basedir + '/public/photo/' + mockOnePhotoData.name, expect.any(Function));
      expect(updateMock).toBeCalledWith(
        {
          name: 'photo-1234567890.jpg',
          originalName: 'photo.jpg',
          description: 'Photo description',
          photoPath: 'public/photo/photo-1234567890.jpg',
          size: 12345,
        },
        { where: { id: 1 } }
      );
      expect(res.status).toBeCalledWith(200);
      expect(res.json).toBeCalledWith({
        message: 'Data Changed Successfully',
        data: mockNewPhotoData,
      });

      findOneMock.mockRestore();
      unlinkMock.mockRestore();
      updateMock.mockRestore();
    });

    test('Should return 401, Data not found', async () => {
      const req = {
        params: {
          id: 1,
        },
        body: {
          name: 'photo-1234567890.jpg',
          description: 'Photo description',
        },
        file: {
          filename: 'photo-1234567890.jpg',
          originalname: 'photo.jpg',
          path: 'public/photo/photo-1234567890.jpg',
          size: 12345,
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const findOneMock = jest.spyOn(PhotoMedia, 'findOne').mockResolvedValue(null);

      await PhotoController.updates(req, res);

      expect(findOneMock).toBeCalledWith({ where: { id: 1 } });
      expect(res.status).toBeCalledWith(401);
      expect(res.json).toBeCalledWith({
        error: {
          message: 'Data not found',
          data: {},
        },
      });

      findOneMock.mockRestore();
    });

    test('Should return 400, Server Error', async () => {
      const req = {
        params: {
          id: 1,
        },
        body: {
          name: 'photo-1234567890.jpg',
          description: 'Photo description',
        },
        file: {
          filename: 'photo-1234567890.jpg',
          originalname: 'photo.jpg',
          path: 'public/photo/photo-1234567890.jpg',
          size: 12345,
        },
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const findOneMock = jest.spyOn(PhotoMedia, 'findOne').mockImplementation(() => {
        throw new Error('Server Error');
      });

      await PhotoController.updates(req, res);

      expect(findOneMock).toBeCalledWith({ where: { id: 1 } });
      expect(res.status).toBeCalledWith(400);
      expect(res.json).toBeCalledWith({
        error: {
          data: 'Server Error',
          message: 'Server Error',
        },
      });

      findOneMock.mockRestore();
    });
  });
});
