const userProfileController = require('../controllers/userProfile.controller');
const { user_profile } = require('../models/');

// Mock request and response objects
const req = {};
const res = {
  status: jest.fn().mockReturnThis(),
  json: jest.fn(),
};

// Positive test cases
describe('userProfileController', () => {
  describe('index', () => {
    it('should retrieve all user profiles and return success response', async () => {
      const mockUserProfiles = {
        id: 1,
        firstname: 'John',
        lastname: 'Doe',
      };
      user_profile.findAll = jest.fn().mockResolvedValue(mockUserProfiles);

      await userProfileController.index(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith({
        data: mockUserProfiles,
        message: 'user profile',
      });
    });
  });

  describe('create', () => {
    it('should create a new user profile and return success response', async () => {
      const mockUserProfile = {
        id: 1,
        firstname: 'John',
        lastname: 'Doe',
      };
      user_profile.create = jest.fn().mockResolvedValue(mockUserProfile);

      req.body = {
        firstname: 'John',
        lastname: 'Doe',
        bio: 'Some bio',
        city: 'Some city',
        social_media_url: 'https://example.com',
      };

      await userProfileController.create(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
    });
  });

  describe('updates', () => {
    it('should update an existing user profile and return success response', async () => {
      const mockUpdatedUserProfile = {
        id: 1,
        firstname: 'John',
        lastname: 'Doe',
        bio: 'Updated bio',
        city: 'Some city',
        social_media_url: 'https://example.com',
      };
      user_profile.update = jest.fn().mockResolvedValue([1]);

      req.params = {
        id: 1,
      };
      req.body = {
        firstname: 'John',
        lastname: 'Doe',
        bio: 'Updated bio',
        city: 'Some city',
        social_media_url: 'https://example.com',
      };

      await userProfileController.updates(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
    });

    it('should handle error when updating user profile', async () => {
      const mockError = new Error('Server Error');
      user_profile.update = jest.fn().mockRejectedValue(mockError);

      req.params = {
        id: 1,
      };
      req.body = {
        firstname: 'John',
        lastname: 'Doe',
        bio: 'Updated bio',
        city: 'Some city',
        social_media_url: 'https://example.com',
      };

      await userProfileController.updates(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
    });
  });

  describe('deletes', () => {
    it('should delete a user profile and return success response', async () => {
      const mockDeletedUserProfile = {
        id: 1,
        firstname: 'John',
        lastname: 'Doe',
      };
      user_profile.destroy = jest.fn().mockResolvedValue(1);

      req.params = {
        id: 1,
      };

      await userProfileController.deletes(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
    });

    it('should handle error when deleting user profile', async () => {
      const mockError = new Error('Server Error');
      user_profile.destroy = jest.fn().mockRejectedValue(mockError);

      req.params = {
        id: 1,
      };

      await userProfileController.deletes(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
    });
  });
});
